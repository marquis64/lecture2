package sbp.branching;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import sbp.common.Utils;

import static org.mockito.ArgumentMatchers.anyString;

public class MyBranchingTests {

    Utils utilsMock = Mockito.mock(Utils.class);
    MyBranching myBranching = new MyBranching(utilsMock);

    @Test
    public void maxInt_Test() {

        Mockito.when(utilsMock.utilFunc1(anyString())).thenReturn(false);
        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);

        Assertions.assertEquals(3, myBranching.maxInt(2,3)); //3
        Assertions.assertEquals(3, myBranching.maxInt(3,2)); //3

        System.out.println(myBranching.maxInt(3,2));
        System.out.println(myBranching.maxInt(2,3));

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        Assertions.assertEquals(0, myBranching.maxInt(2,3)); //0

        System.out.println(myBranching.maxInt(2,3));
    }

    @Test
    public void ifElseExample_Test() {

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Assertions.assertTrue(myBranching.ifElseExample());

        Mockito.when(utilsMock.utilFunc2()).thenReturn(false);
        Assertions.assertFalse(myBranching.ifElseExample());

    }

    @Test
    public void switchExample_0_Test() {

        //При 0

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);
        Mockito.when(utilsMock.utilFunc1("abc2")).thenReturn(true);

        myBranching.switchExample(0);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1("abc2");

    }

    @Test
    public void switchExample_1_Test() {

        //При 1

        Mockito.when(utilsMock.utilFunc1("abc")).thenReturn(true);

        myBranching.switchExample(1);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc1("abc");
        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();


    }

    @Test
    public void switchExample_2_Test() {

        //При 2

        Mockito.when(utilsMock.utilFunc2()).thenReturn(true);

        myBranching.switchExample(2);

        Mockito.verify(utilsMock, Mockito.times(1)).utilFunc2();
        Mockito.verify(utilsMock, Mockito.times(0)).utilFunc1(anyString());

    }
}